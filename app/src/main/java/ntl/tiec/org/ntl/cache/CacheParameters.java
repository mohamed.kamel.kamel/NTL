package ntl.tiec.org.ntl.cache;

import android.content.Context;
import android.content.SharedPreferences;

public class CacheParameters {
    public static final String PREFERENCES_NAME = CacheParameters.class.getPackage().getName();
    private static final String code = "code", governorate = "governorate", center = "center",
            round = "round", cohort = "cohort", track = "track", fullName = "fullName", email = "Email",
            centerLat = "centerLat", centerLng = "centerLng", mobileSerial = "mobileSerial", signType = "signType";
    private SharedPreferences preferences = null;

//    public static SharedPreferences getSharedPreferences(Context context) {
//        return context.getSharedPreferences(PREFERENCES_NAME,context.MODE_PRIVATE);
//    }

    public CacheParameters(Context context) {
        this.preferences = context.getSharedPreferences(PREFERENCES_NAME,
                context.MODE_PRIVATE);
    }

    public String getCenterLat() {
        return this.preferences.getString(centerLat, null);
    }

    public void setCenterLat(String centerLat) {
        this.preferences.edit().putString(this.centerLat, centerLat).commit();
    }

    public String getCenterLng() {
        return this.preferences.getString(centerLng, null);
    }

    public void setCenterLng(String centerLng) {
        this.preferences.edit().putString(this.centerLng, centerLng).commit();
    }

    public String getCode() {
        return this.preferences.getString(code, null);
    }

    public void setCode(String code) {
        this.preferences.edit().putString(this.code, code).commit();
    }

    public String getGovernorate() {
        return this.preferences.getString(governorate, null);
    }

    public void setGovernorate(String governorate) {
        this.preferences.edit().putString(this.governorate, governorate).commit();
    }

    public String getCenter() {
        return this.preferences.getString(center, null);
    }

    public void setCenter(String center) {
        this.preferences.edit().putString(this.center, center).commit();
    }

    public String getRound() {
        return this.preferences.getString(round, null);
    }

    public void setRound(String round) {
        this.preferences.edit().putString(this.round, round).commit();
    }

    public String getCohort() {
        return this.preferences.getString(cohort, null);
    }

    public void setCohort(String cohort) {
        this.preferences.edit().putString(this.cohort, cohort).commit();
    }

    public String getTrack() {
        return this.preferences.getString(track, null);
    }

    public void setTrack(String track) {
        this.preferences.edit().putString(this.track, track).commit();
    }

    public String getFullName() {
        return this.preferences.getString(fullName, null);
    }

    public void setFullName(String fullName) {
        this.preferences.edit().putString(this.fullName, fullName).commit();
    }

    public String getEmail() {
        return this.preferences.getString(email, null);
    }

    public void setEmail(String email) {
        this.preferences.edit().putString(this.email, email).commit();
    }

    public String getMobileSerial() {
        return this.preferences.getString(mobileSerial, null);
    }

    public void setMobileSerial(String mobileSerial) {
        this.preferences.edit().putString(this.mobileSerial, mobileSerial).commit();
    }

    public String getSignType() {
        return this.preferences.getString(signType, null);
    }

    public void setSignType(String signType) {
        this.preferences.edit().putString(this.signType, signType).commit();
    }
}
