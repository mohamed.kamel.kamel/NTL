package ntl.tiec.org.ntl.model;

import android.app.Application;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import ntl.tiec.org.ntl.cache.CacheParameters;

@Module
public class AppModule {
    Application mApplication;

    public AppModule(Application application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

}
