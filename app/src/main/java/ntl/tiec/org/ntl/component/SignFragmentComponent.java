package ntl.tiec.org.ntl.component;

import android.app.Fragment;

import javax.inject.Singleton;

import dagger.Component;
import ntl.tiec.org.ntl.model.AppModule;
import ntl.tiec.org.ntl.model.SignFragmentModule;
import ntl.tiec.org.ntl.ui.PlaceholderFragment;

@Singleton
@Component(modules = {AppModule.class, SignFragmentModule.class})
public interface SignFragmentComponent {
    void inject(PlaceholderFragment signFragment);
}
