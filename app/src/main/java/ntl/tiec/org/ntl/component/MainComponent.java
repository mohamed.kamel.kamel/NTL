package ntl.tiec.org.ntl.component;

import javax.inject.Singleton;

import dagger.Component;
import ntl.tiec.org.ntl.model.AppModule;
import ntl.tiec.org.ntl.model.MainModule;
import ntl.tiec.org.ntl.ui.MainActivity;

@Singleton
@Component(modules = {AppModule.class, MainModule.class})
public interface MainComponent {
    void inject(MainActivity activityMain);
}
