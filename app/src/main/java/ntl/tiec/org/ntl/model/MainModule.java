package ntl.tiec.org.ntl.model;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ntl.tiec.org.ntl.cache.CacheParameters;
import ntl.tiec.org.ntl.firebase.FirebaseEngine;

@Module
public class MainModule {

    private final Context context;

    public MainModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context provideMainContext() {
        return this.context;
    }

    @Provides
    public CacheParameters provideMainPrefrences(Context context) {
        return new CacheParameters(context);
    }

    @Provides
    @Singleton
    public FirebaseEngine provideMainFirebaseEngine(Context context) {
        return new FirebaseEngine(context);
    }
}
