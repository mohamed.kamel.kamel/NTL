package ntl.tiec.org.ntl;

import android.support.multidex.MultiDexApplication;

import ntl.tiec.org.ntl.component.DaggerLoginComponent;
import ntl.tiec.org.ntl.component.DaggerMainComponent;
import ntl.tiec.org.ntl.component.DaggerSignFragmentComponent;
import ntl.tiec.org.ntl.component.LoginComponent;
import ntl.tiec.org.ntl.component.MainComponent;
import ntl.tiec.org.ntl.component.SignFragmentComponent;
import ntl.tiec.org.ntl.model.AppModule;
import ntl.tiec.org.ntl.model.LoginModule;
import ntl.tiec.org.ntl.model.MainModule;
import ntl.tiec.org.ntl.model.SignFragmentModule;

public class NTL_App extends MultiDexApplication {
    private LoginComponent studentComponent;
    private MainComponent mainComponent;
    private SignFragmentComponent signFragmentComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        studentComponent = DaggerLoginComponent.builder()
                // list of modules that are part of this component need to be created here too
                .appModule(new AppModule(this)) // This also corresponds to the name of your module: %component_name%Module
                .loginModule(new LoginModule(this))
                .build();

        mainComponent = DaggerMainComponent.builder()
                // list of modules that are part of this component need to be created here too
                .appModule(new AppModule(this)) // This also corresponds to the name of your module: %component_name%Module
                .mainModule(new MainModule(this))
                .build();

        signFragmentComponent = DaggerSignFragmentComponent.builder()
                // list of modules that are part of this component need to be created here too
                .appModule(new AppModule(this)) // This also corresponds to the name of your module: %component_name%Module
                .signFragmentModule(new SignFragmentModule(this))
                .build();
    }

    public LoginComponent getLoginComponent() {
        return studentComponent;
    }

    public MainComponent getMainComponent() {
        return mainComponent;
    }

    public SignFragmentComponent getSignFragmentComponent() {
        return signFragmentComponent;
    }
}
