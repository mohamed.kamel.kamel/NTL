package ntl.tiec.org.ntl.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ntl.tiec.org.ntl.NTL_App;
import ntl.tiec.org.ntl.R;
import ntl.tiec.org.ntl.cache.CacheParameters;
import ntl.tiec.org.ntl.firebase.FirebaseEngine;


public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.code_login)
    TextView codeLogin;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.email_login)
    TextView emailLogin;

    @Inject
    CacheParameters cacheParameters;
    @Inject
    FirebaseEngine firebaseEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((NTL_App) getApplication()).getLoginComponent().inject(this);

        if (cacheParameters.getCode() == null)
            drawGUI();
        else {
            Toast.makeText(this, getResources().getString(R.string.welcome) + cacheParameters.getFullName(), Toast.LENGTH_LONG).show();
            firebaseEngine.login(LoginActivity.this, cacheParameters.getCode(), cacheParameters.getEmail(), cacheParameters);
        }

    }

    private void drawGUI() {
        this.setContentView(R.layout.login);
        ButterKnife.bind(this);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String learnerCode = codeLogin.getText().toString();
                String emailCode = emailLogin.getText().toString();

                if (learnerCode.equals("") || emailLogin.equals("")) {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.loginError), Toast.LENGTH_SHORT).show();
                    return;
                }
                firebaseEngine.login(LoginActivity.this, learnerCode, emailCode, cacheParameters);
            }
        });

    }
}
