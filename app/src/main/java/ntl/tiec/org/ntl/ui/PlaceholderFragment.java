package ntl.tiec.org.ntl.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import butterknife.ButterKnife;
import ntl.tiec.org.ntl.NTL_App;
import ntl.tiec.org.ntl.R;
import ntl.tiec.org.ntl.cache.CacheParameters;
import ntl.tiec.org.ntl.firebase.FirebaseEngine;
import ntl.tiec.org.ntl.utils.CalcDistance;
import ntl.tiec.org.ntl.utils.GPSTracker;

public class PlaceholderFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";

    @Inject
    CacheParameters cacheParameters;
    @Inject
    FirebaseEngine firebaseEngine;

    GPSTracker gps;
    double latitude = 0;
    double longitude = 0;

    TextView signType, signDatetime, signNote;

    public PlaceholderFragment() {
    }

    public static PlaceholderFragment newInstance(int sectionNumber, Activity activity) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        ((NTL_App) activity.getApplication()).getSignFragmentComponent().inject(fragment);
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        int tabNum = getArguments().getInt(ARG_SECTION_NUMBER);
        if (tabNum == 1) {
            View rootView = inflater.inflate(R.layout.sign, container, false);
            buildTab1(rootView);
            return rootView;
        } else if (tabNum == 2) {
            View rootView = inflater.inflate(R.layout.attendance_list, container, false);
            buildTab2(rootView);
            return rootView;
        } else if (tabNum == 3) {
            View rootView = inflater.inflate(R.layout.info_layout, container, false);
            ButterKnife.bind(rootView);
            buildTab3(rootView);
            return rootView;
        } else {
            return null;
        }
    }

    private void buildTab3(View rootView) {
        TextView dateActionbar = (TextView) rootView.findViewById(R.id.date_actionbar);
        TextView learnerActionbar = (TextView) rootView.findViewById(R.id.learner_actionbar);
        TextView nameActionbar = (TextView) rootView.findViewById(R.id.name_actionbar);
        TextView roundActionbar = (TextView) rootView.findViewById(R.id.round_actionbar);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = simpleDateFormat.format(new Date());
        dateActionbar.setText(dateActionbar.getText() + currentDate);
        nameActionbar.setText(nameActionbar.getText() + cacheParameters.getFullName());
        learnerActionbar.setText(learnerActionbar.getText() + cacheParameters.getCode());
        roundActionbar.setText(roundActionbar.getText() + cacheParameters.getRound());
    }

    private void buildTab2(View rootView) {
        RecyclerView attendanceList = (RecyclerView) rootView.findViewById(R.id.attendance);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        attendanceList.setLayoutManager(layoutManager);
        firebaseEngine.listAttendanceDays(cacheParameters, attendanceList);
    }

    private void buildTab1(View rootView) {
        ImageView signBtn = (ImageView) rootView.findViewById(R.id.sign_btn);
        LinearLayout commentLayout = (LinearLayout) rootView.findViewById(R.id.comment_layout);
        signType = (TextView) rootView.findViewById(R.id.sign_type_txt);
        signDatetime = (TextView) rootView.findViewById(R.id.sign_datetime);
        signNote = (TextView) rootView.findViewById(R.id.sign_note);
        firebaseEngine.isSigned(cacheParameters.getCode(), signType, signDatetime, signBtn, commentLayout, signNote,cacheParameters);

        signBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signActionPreformed();
            }
        });
        final EditText commentTxt = (EditText) rootView.findViewById(R.id.comment_txt);
        ImageButton commentSendBtn = (ImageButton) rootView.findViewById(R.id.comment_send_btn);

        commentSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentTxt.getText().toString().equals(""))
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.commentInputErro),
                            Toast.LENGTH_LONG).show();
                else
                    firebaseEngine.insertComment(cacheParameters, commentTxt);
            }
        });
    }

    private void signActionPreformed() {
        gps = new GPSTracker(getContext());
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            if (CalcDistance.isFraudDetectBySignedLocation(cacheParameters, latitude, longitude)) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle(getContext().getResources().getString(R.string.fraudSign));
                alertDialog.setMessage(getContext().getResources().getString(R.string.sendusemail));
                alertDialog.create().show();
            } else {
                String mobileSerialNumber = Settings.Secure.getString(getActivity().getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                if (cacheParameters.getSignType() != null && cacheParameters.getSignType().equals("signed")) {
                    if (!CalcDistance.isFraudDetectByMobileSerialNumber(cacheParameters.getMobileSerial(), mobileSerialNumber)) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle(getContext().getResources().getString(R.string.fraudSerial));
                        alertDialog.setMessage(getContext().getResources().getString(R.string.sendusemail));
                        alertDialog.create().show();
                        return;
                    }
                }
                callSignProcess(mobileSerialNumber);
            }
        } else {
            gps.showSettingsAlert();
        }
    }

    private void callSignProcess(String mobileSerialNumber) {
        if (signType.getText().toString().equals(getActivity().getResources().getString(R.string.signin)))
            firebaseEngine.signIn(cacheParameters, cacheParameters.getCenter(), signType, signDatetime, latitude, longitude, signNote);
        else if (signType.getText().toString().equals(getActivity().getResources().getString(R.string.signout)))
            firebaseEngine.signOut(cacheParameters, mobileSerialNumber, cacheParameters.getCenter(), signType, signDatetime, latitude, longitude, signNote);
    }
}
