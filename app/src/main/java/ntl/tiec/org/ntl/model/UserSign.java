package ntl.tiec.org.ntl.model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class UserSign {
    public String attendanceDate;
    public String macAddressCheckIn;
    public String checkIn;
    public String signIn;
    public String macAddressCheckOut;
    public String checkOut;
    public String signOut;
    public double latitudeIn;
    public double longitudeIn;
    public double latitudeOut;
    public double longitudeOut;

    public UserSign() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public UserSign(String macAddressCheckIn, String checkIn, String signIn,
                    double latitudeIn, double longitudeIn) {
        this.macAddressCheckIn = macAddressCheckIn;
        this.checkIn = checkIn;
        this.signIn = signIn;
        this.latitudeIn = latitudeIn;
        this.longitudeIn = longitudeIn;
    }

}
