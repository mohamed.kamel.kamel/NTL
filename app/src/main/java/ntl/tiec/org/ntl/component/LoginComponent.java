package ntl.tiec.org.ntl.component;

import javax.inject.Singleton;

import dagger.Component;
import ntl.tiec.org.ntl.model.AppModule;
import ntl.tiec.org.ntl.model.LoginModule;
import ntl.tiec.org.ntl.ui.LoginActivity;

@Singleton
@Component(modules = {AppModule.class, LoginModule.class})
public interface LoginComponent {
    void inject(LoginActivity activityLogin);
}
