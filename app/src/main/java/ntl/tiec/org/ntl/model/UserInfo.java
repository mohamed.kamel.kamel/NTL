package ntl.tiec.org.ntl.model;


import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class UserInfo {

    public String center;
    public String governorate;
    public String cohort;
    public String track;
    public String name;
    public String round;
    public String email;
    public String mobile;
    public String provider;
    public String center_lat;
    public String center_lng;

    public UserInfo() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public UserInfo(String center, String governorate, String cohort,
                    String track, String name, String round,
                    String email, String mobile, String provider,
                    String center_lat, String center_lng) {
        this.center = center;
        this.governorate = governorate;
        this.cohort = cohort;
        this.track = track;
        this.name = name;
        this.round = round;
        this.provider = provider;
        this.email = email;
        this.mobile = mobile;
        this.center_lat = center_lat;
        this.center_lng = center_lng;
    }
}
