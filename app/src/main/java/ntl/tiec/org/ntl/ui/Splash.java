package ntl.tiec.org.ntl.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import ntl.tiec.org.ntl.R;
import ntl.tiec.org.ntl.firebase.FirebaseEngine;

public class Splash extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    TextView textprogs;
    ProgressBar brogs;
    int i = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        textprogs = (TextView) findViewById(R.id.textprogs);
        brogs = (ProgressBar) findViewById(R.id.brogs);

        if (!new FirebaseEngine(getApplicationContext()).isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "No internet Connection!!",
                    Toast.LENGTH_LONG).show();
        } else {
            new CountDownTimer(SPLASH_TIME_OUT, 100) {

                public void onTick(long millisUntilFinished) {

                    i = SPLASH_TIME_OUT - (int) millisUntilFinished;
                    int total = (int) ((i * 100) / SPLASH_TIME_OUT);
                    brogs.setProgress(total);
                    textprogs.setText(total + "%");
                }

                public void onFinish() {
                    brogs.setProgress(100);
                    textprogs.setText(100 + "%");
                    Intent i = new Intent(Splash.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }.start();
        }
    }
}
