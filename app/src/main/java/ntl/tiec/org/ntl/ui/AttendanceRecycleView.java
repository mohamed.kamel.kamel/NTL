package ntl.tiec.org.ntl.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import ntl.tiec.org.ntl.R;
import ntl.tiec.org.ntl.model.UserSign;

/**
 * Created by M Kamel on 7/29/2017.
 */

public class AttendanceRecycleView extends RecyclerView.Adapter<AttendanceRecycleView.ViewHolder> {

    Context context;
    ArrayList<UserSign> userSignArra;

    public AttendanceRecycleView(Context context, ArrayList<UserSign> userSignArra) {
        this.context = context;
        this.userSignArra = userSignArra;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View V = LayoutInflater.from(parent.getContext()).inflate(R.layout.attendance_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(V);
        return viewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ////////////////////////////////set the post data///////////////////////////////////////////////
        holder.date.setText(userSignArra.get(position).attendanceDate);
        if (userSignArra.get(position).signOut == null)
            holder.time.setText(userSignArra.get(position).signIn);
        else
            holder.time.setText(userSignArra.get(position).signIn + " - " + userSignArra.get(position).signOut);
        holder.address.setText(userSignArra.get(position).checkIn);
        if (position % 2 != 0) {
            holder.attendance_date_time.setBackgroundColor(R.color.rowDark);
        }
    }

    @Override
    public int getItemCount() {
        return userSignArra.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView date, time, address;
        LinearLayout attendance_date_time;

        public ViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            time = (TextView) itemView.findViewById(R.id.time);
            address = (TextView) itemView.findViewById(R.id.address);
            attendance_date_time = (LinearLayout) itemView.findViewById(R.id.attendance_date_time);
        }
    }
}
