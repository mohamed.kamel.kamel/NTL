package ntl.tiec.org.ntl.ui;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    Activity activity;
    public SectionsPagerAdapter(FragmentManager fm,Activity activity) {
        super(fm);
        this.activity=activity;
    }

    @Override
    public Fragment getItem(int position) {
        return PlaceholderFragment.newInstance(position + 1,activity);
    }

    @Override
    public int getCount() {
        return 3;
    }
}
