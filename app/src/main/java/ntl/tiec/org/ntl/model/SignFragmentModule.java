package ntl.tiec.org.ntl.model;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ntl.tiec.org.ntl.cache.CacheParameters;
import ntl.tiec.org.ntl.firebase.FirebaseEngine;

@Module
public class SignFragmentModule {
    private final Context context;

    public SignFragmentModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context provideContext() {
        return this.context;
    }

    @Provides
    public CacheParameters providePrefrences(Context context) {
        return new CacheParameters(context);
    }

    @Provides
    @Singleton
    public FirebaseEngine provideFirebaseEngine(Context context) {
        return new FirebaseEngine(context);
    }
}
