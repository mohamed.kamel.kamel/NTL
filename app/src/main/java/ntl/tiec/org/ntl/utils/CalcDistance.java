package ntl.tiec.org.ntl.utils;

import android.util.Log;

import ntl.tiec.org.ntl.cache.CacheParameters;

public class CalcDistance {

    public static boolean isFraudDetectByMobileSerialNumber(String serialSignIn, String serialSignOut) {
        return serialSignIn.equals(serialSignOut);
    }

    public static boolean isFraudDetectBySignedLocation(CacheParameters cacheParameters, double latitude, double longitude) {
        double fraudDistance = distance(Double.parseDouble(cacheParameters.getCenterLat()),
                Double.parseDouble(cacheParameters.getCenterLng()),
                latitude, longitude);
        Log.d(cacheParameters.getCenterLat() + "fraudDistance", cacheParameters.getCenterLng());
        Log.d(latitude + "fraudDistance", longitude + "");
        Log.d("fraudDistance", fraudDistance + "");
        if (fraudDistance > 1.0) {
            return true;
        }
        return false;
    }

    /**
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     * <p>
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
     * el2 End altitude in meters
     *
     * @returns Distance in Kilo Meters
     */
    private static double distance(double lat1, double lng1, double lat2, double lng2) {

        double earthRadius = 6371; // earth for kilometers

        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);

        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);

        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double dist = earthRadius * c;

        return dist;
    }

}
