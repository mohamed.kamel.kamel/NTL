package ntl.tiec.org.ntl.firebase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ntl.tiec.org.ntl.R;
import ntl.tiec.org.ntl.cache.CacheParameters;
import ntl.tiec.org.ntl.model.UserInfo;
import ntl.tiec.org.ntl.model.UserSign;
import ntl.tiec.org.ntl.ui.AttendanceRecycleView;
import ntl.tiec.org.ntl.ui.LoginActivity;
import ntl.tiec.org.ntl.ui.MainActivity;

public class FirebaseEngine {
    DatabaseReference myFirebaseRef;
    Context context;
    boolean isHasSession = false;
    String macAddressCheckIn = "";

    public FirebaseEngine(Context context) {
        this.myFirebaseRef = FirebaseDatabase.getInstance().getReference();
        this.context = context;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void login(final Activity act, final String learnerCode,
                      final String learnerEmail, final CacheParameters cacheParameters) {
        myFirebaseRef.child("learners").child(learnerCode).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                UserInfo userInfo = snapshot.getValue(UserInfo.class);
                if (userInfo != null && userInfo.email.equals(learnerEmail)) {
                    cacheParameters.setCode(learnerCode);
                    cacheParameters.setEmail(learnerEmail);
                    cacheParameters.setCenter(userInfo.center);
                    cacheParameters.setCohort(userInfo.cohort);
                    cacheParameters.setGovernorate(userInfo.governorate);
                    cacheParameters.setFullName(userInfo.name);
                    cacheParameters.setRound(userInfo.round);
                    cacheParameters.setTrack(userInfo.track);
                    cacheParameters.setCenterLat(userInfo.center_lat);
                    cacheParameters.setCenterLng(userInfo.center_lng);
                    Toast.makeText(context, context.getResources().getString(R.string.welcome) + userInfo.name, Toast.LENGTH_SHORT).show();
                    context.startActivity(new Intent(context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                } else {
                    Toast.makeText(context, context.getResources().getString(R.string.loginfailed), Toast.LENGTH_SHORT).show();
                    context.startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                }
                act.finish();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(context, context.getResources().getString(R.string.firebaseErrorRead) + databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void signIn(final CacheParameters cacheParameters,
                       String checkIn, TextView signType, TextView signDate,
                       double latitude, double longitude, TextView signNote) {
        String macAddress = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        cacheParameters.setMobileSerial(macAddress);
        cacheParameters.setSignType("signed");
        String signInDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String signIn = new SimpleDateFormat("hh:mm a").format(new Date());
        UserSign userSignIn = new UserSign(macAddress, checkIn, signIn, latitude, longitude);
        myFirebaseRef.child("UsersAttendance").child(cacheParameters.getCode()).child(signInDate).
                setValue(userSignIn);
        myFirebaseRef.child("StudyGroup").child(signInDate).child(cacheParameters.getGovernorate()).child(cacheParameters.getCenter())
                .child(cacheParameters.getTrack()).child(cacheParameters.getCode()).setValue(userSignIn);
        signType.setText(context.getResources().getString(R.string.signout));
        signDate.setText("Sign In at: " + signIn);
        signDate.setVisibility(View.VISIBLE);
        signNote.setText(context.getResources().getString(R.string.signoutnote));
    }

    public void signOut(final CacheParameters cacheParameters, final String macAddress,
                        String checkOut, TextView signType, TextView signDate,
                        double latitude, double longitude, TextView signNote) {
        cacheParameters.setMobileSerial(null);
        cacheParameters.setSignType(null);
        final String signOutDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String signOut = new SimpleDateFormat("hh:mm a").format(new Date());
        myFirebaseRef.child("UsersAttendance").child(cacheParameters.getCode()).child(signOutDate).
                child("macAddressCheckOut").setValue(macAddress);
        myFirebaseRef.child("StudyGroup").child(signOutDate).child(cacheParameters.getGovernorate()).child(cacheParameters.getCenter())
                .child(cacheParameters.getTrack()).child(cacheParameters.getCode()).
                child("macAddressCheckOut").setValue(macAddress);
        myFirebaseRef.child("UsersAttendance").child(cacheParameters.getCode()).child(signOutDate).
                child("checkOut").setValue(checkOut);
        myFirebaseRef.child("StudyGroup").child(signOutDate).child(cacheParameters.getGovernorate()).child(cacheParameters.getCenter())
                .child(cacheParameters.getTrack()).child(cacheParameters.getCode()).
                child("checkOut").setValue(checkOut);
        myFirebaseRef.child("UsersAttendance").child(cacheParameters.getCode()).child(signOutDate).
                child("signOut").setValue(signOut);
        myFirebaseRef.child("StudyGroup").child(signOutDate).child(cacheParameters.getGovernorate()).child(cacheParameters.getCenter())
                .child(cacheParameters.getTrack()).child(cacheParameters.getCode()).
                child("signOut").setValue(signOut);
        myFirebaseRef.child("UsersAttendance").child(cacheParameters.getCode()).child(signOutDate).
                child("latitudeOut").setValue(latitude);
        myFirebaseRef.child("StudyGroup").child(signOutDate).child(cacheParameters.getGovernorate()).child(cacheParameters.getCenter())
                .child(cacheParameters.getTrack()).child(cacheParameters.getCode()).
                child("latitudeOut").setValue(latitude);
        myFirebaseRef.child("UsersAttendance").child(cacheParameters.getCode()).child(signOutDate).
                child("longitudeOut").setValue(longitude);
        myFirebaseRef.child("StudyGroup").child(signOutDate).child(cacheParameters.getGovernorate()).child(cacheParameters.getCenter())
                .child(cacheParameters.getTrack()).child(cacheParameters.getCode()).
                child("longitudeOut").setValue(longitude);
        signType.setVisibility(View.GONE);
        signNote.setText(context.getResources().getString(R.string.goodbye));
    }

    public boolean isLogin(final String learnerCode) {
        myFirebaseRef.child("learners").child(learnerCode).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                UserInfo userInfo = snapshot.getValue(UserInfo.class);
                if (userInfo != null) {
                    isHasSession = true;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return isHasSession;
    }

    public void isSigned(final String learnerCode, final TextView signType,
                         final TextView signDatetime, final ImageView signBtn,
                         final LinearLayout commentLayout, final TextView signNote,
                         final CacheParameters cacheParameters) {
        String signInDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        myFirebaseRef.child("UsersAttendance").child(learnerCode).child(signInDate)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        UserSign userSign = snapshot.getValue(UserSign.class);
                        if (userSign == null) {
                            signNote.setText(context.getResources().getString(R.string.siginnote));
                            signType.setText(context.getResources().getString(R.string.signin));
                            signDatetime.setVisibility(View.GONE);
                            commentLayout.setVisibility(View.GONE);
                            cacheParameters.setMobileSerial(null);
                            cacheParameters.setSignType(null);
                        } else if (userSign.signOut == null) {
                            signNote.setText(context.getResources().getString(R.string.signoutnote));
                            signType.setText(context.getResources().getString(R.string.signout));
                            cacheParameters.setMobileSerial(userSign.macAddressCheckIn);
                            cacheParameters.setSignType("signed");
                            signDatetime.setText("Sign In at: " + userSign.signIn);
                            signDatetime.setVisibility(View.VISIBLE);
                            commentLayout.setVisibility(View.GONE);
                        } else {
                            signNote.setText(context.getResources().getString(R.string.goodbye));
                            signDatetime.setVisibility(View.GONE);
                            signBtn.setVisibility(View.GONE);
                            commentLayout.setVisibility(View.VISIBLE);
                            cacheParameters.setMobileSerial(null);
                            cacheParameters.setSignType(null);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void listAttendanceDays(CacheParameters cacheParameters, final RecyclerView attendanceListView) {
        final ArrayList<UserSign> userSignArray = new ArrayList<>();
        myFirebaseRef.child("UsersAttendance").child(cacheParameters.getCode())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        userSignArray.clear();
                        for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                            UserSign userSign = postSnapshot.getValue(UserSign.class);
                            userSign.attendanceDate = postSnapshot.getKey();
                            userSign.signOut = (String) postSnapshot.child("signOut").getValue();
                            userSignArray.add(userSign);
                            System.out.println(userSign.signOut);
                        }
                        AttendanceRecycleView attendanceRecycleView = new AttendanceRecycleView(context, userSignArray);
                        attendanceListView.setAdapter(attendanceRecycleView);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(context, context.getResources().getString(R.string.firebaseErrorRead) + databaseError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void insertComment(final CacheParameters cacheParameters, EditText commentTxt) {
        String commentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        myFirebaseRef.child("StudyGroup").child(commentDate).child(cacheParameters.getGovernorate()).
                child(cacheParameters.getCenter()).child(cacheParameters.getTrack()).
                child(cacheParameters.getCode()).child("Comments").
                setValue(commentTxt.getText().toString());
        commentTxt.setText("");
        Toast.makeText(context, context.getResources().getString(R.string.commentSend), Toast.LENGTH_LONG).show();
    }
}
